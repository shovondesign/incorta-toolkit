<?php if (!defined('ABSPATH')) die('-1');

vc_map(
	array(
		"name" => esc_html__( "incorta Button", "incorta-toolkit" ),
		"base" => "incorta_btn",
		"category" => esc_html__( "Incorta Addons", "incorta-toolkit"),
		"params" => array(
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Button Text", "incorta-toolkit" ),
				"param_name" => "btn_text",
				"value" => esc_html__( "Button", "incorta-toolkit" ),
				"description" => esc_html__( "Type button text here.", "incorta-toolkit" )
			),
			array(
				"type" => "iconpicker",
				"heading" => esc_html__( "Choose Icon", "incorta-toolkit" ),
				"param_name" => "btn_icon",
				"description" => esc_html__( "Choose a icon from here.", "incorta-toolkit" )
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Button Link", "incorta-toolkit" ),
				"param_name" => "btn_link",
				"value" => esc_html__( "#", "incorta-toolkit" ),
				"description" => esc_html__( "Type button link here.", "incorta-toolkit" )
			),
			array(
				"type"		=> "dropdown",
				"param_name" => "btn_type",
				"heading"	=> esc_html__( "Select Button Type", "incorta-toolkit" ),
				'value'		=> array(
					'Normal Button' => '',
					'Youtube Button'	=> 'youtube-btn',
					'Blueprint Button'	=> 'blueprints-btn',
				),
			),
		)
	)
);