<?php if (!defined('ABSPATH')) die('-1');

vc_map(
	array(
		"name" => esc_html__( "Platform Addon", "incorta-toolkit" ),
		"base" => "incorta_platform",
		"category" => esc_html__( "Incorta Addons", "incorta-toolkit"),
		"params" => array(
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Platform Heading", "incorta-toolkit" ),
				"param_name" => "platform_heading",
				"value" => esc_html__( "Heading", "incorta-toolkit" ),
				"description" => esc_html__( "Type platform heading here.", "incorta-toolkit" )
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Platform Sub-Heading", "incorta-toolkit" ),
				"param_name" => "platform_subheading",
				"value" => esc_html__( "Sub-Heading", "incorta-toolkit" ),
				"description" => esc_html__( "Type platform sub-heading here.", "incorta-toolkit" )
			),
			array(
				"type" => "attach_image",
				"heading" => esc_html__( "Platform Image", "incorta-toolkit" ),
				"param_name" => "platform_img",
				"description" => esc_html__( "Upload platform image here.", "incorta-toolkit")
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Platform Details", "incorta-toolkit" ),
				"param_name" => "platform_details",
				"value" => esc_html__( "Details", "incorta-toolkit" ),
				"description" => esc_html__( "Type platform details here.", "incorta-toolkit" )
			),
		)
	)
);