<?php if (!defined('ABSPATH')) die('-1');

vc_map(
	array(
		"name" => esc_html__( "Home Benner Section", "incorta-toolkit" ),
		"base" => "incorta_home_banner",
		"category" => esc_html__( "Incorta Addons", "incorta-toolkit"),
		"params" => array(
			array(
				"type" => "attach_image",
				"heading" => esc_html__( "Home Benner Background Image", "incorta-toolkit" ),
				"param_name" => "home_banner_bg",
				"description" => esc_html__( "Upload home benner background image here.", "incorta-toolkit")
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Home Benner Title", "incorta-toolkit" ),
				"param_name" => "sec_title",
				"description" => esc_html__( "Type section title here.", "incorta-toolkit"),
			),
			array(
				"type" => "textarea",
				"heading" => esc_html__( "Home Benner Sub-Title", "incorta-toolkit" ),
				"param_name" => "sec_subtitle",
				"description" => esc_html__( "Type section sub-title here.", "incorta-toolkit"),
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Youtube Video Code", "incorta-toolkit" ),
				"param_name" => "youtube_embed_code",
				"value" => "RYvNSA1ww6g",
				"description" => esc_html__( "Copy youtube video code from youtube video url end.", "incorta-toolkit")
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Mailchimp Form Shortcode ID", "incorta-toolkit" ),
				"param_name" => "mc_shortcode_get_id",
				"description" => esc_html__( "Enter Mailchimp Form shortcode ID number.", "incorta-toolkit")
			),
		)
	)
);