<?php if (!defined('ABSPATH')) die('-1');

vc_map(
	array(
		"name" => esc_html__( "Logo Carousel", "incorta-toolkit" ),
		"base" => "incorta_logo_carousel",
		"category" => esc_html__( "Incorta Addons", "incorta-toolkit"),
		"params" => array(
			array(
				"type" => "textfield",
				"param_name" => "sec_title",
				"heading" => esc_html__( "Section Title", "incorta-toolkit" ),
				"description" => esc_html__( "Type section title here.", "incorta-toolkit")
			),
			array(
				"type" => "attach_images",
				"param_name" => "logos",
				"heading" => esc_html__( "Upload logos", "incorta-toolkit" ),
				"description" => esc_html__( "Upload your logos here.", "incorta-toolkit")
			),
		)
	)
);