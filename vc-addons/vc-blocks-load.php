<?php

if (!defined('ABSPATH')) die('-1');

// Class started
class incortaVCExtendAddonClass {

    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'incortaIntegrateWithVC' ) );
    }
 
    public function incortaIntegrateWithVC() {
        // Checks if Visual composer is not installed
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            add_action('admin_notices', array( $this, 'incortaShowVcVersionNotice' ));
            return;
        }
 

        // visual composer addons.
        include INCORTA_ACC_PATH . '/vc-addons/vc-home-benner.php';
        include INCORTA_ACC_PATH . '/vc-addons/vc-platform.php';
        include INCORTA_ACC_PATH . '/vc-addons/vc-btn.php';
        include INCORTA_ACC_PATH . '/vc-addons/vc-testimonial.php';
        include INCORTA_ACC_PATH . '/vc-addons/vc-logo-carousel.php';

    }

    // show visual composer version
    public function incortaShowVcVersionNotice() {
        $theme_data = wp_get_theme();
        echo '
        <div class="notice notice-warning">
          <p>'.sprintf(__('<strong>%s</strong> recommends <strong><a href="'.site_url().'/wp-admin/themes.php?page=tgmpa-install-plugins" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'blacki-toolkit'), $theme_data->get('Name')).'</p>
        </div>';
    }
}
// Finally initialize code
new incortaVCExtendAddonClass();