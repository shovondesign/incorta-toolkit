<?php if (!defined('ABSPATH')) die('-1');

vc_map(
	array(
		"name"		=> esc_html__( "Testimonial Addon", "incorta-toolkit" ),
		"base"		=> "incorta_testimonial",
		"category"	=> esc_html__( "Incorta Addons", "incorta-toolkit"),
		"params"	=> array(			
			array(
				"type" => "attach_image",
				"heading" => esc_html__( "Testimonial Background Image", "incorta-toolkit" ),
				"param_name" => "testimonial_bg",
				"description" => esc_html__( "Upload testimonial background image here.", "incorta-toolkit")
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Before Title", "incorta-toolkit" ),
				"param_name" => "before_incorta_title",
				"value" => esc_html__( "Before Title", "incorta-toolkit" ),
				"description" => esc_html__( "Type before title here.", "incorta-toolkit" )
			),
			array(
				"type" => "textarea",
				"heading" => esc_html__( "Before Details Text", "incorta-toolkit" ),
				"param_name" => "before_incorta_desc",
				"value" => esc_html__( "Before Details", "incorta-toolkit" ),
				"description" => esc_html__( "Type before details here.", "incorta-toolkit" )
			),
			array(
				"type" => "textfield",
				"heading" => esc_html__( "After Title", "incorta-toolkit" ),
				"param_name" => "after_incorta_title",
				"value" => esc_html__( "After Title", "incorta-toolkit" ),
				"description" => esc_html__( "Type After title here.", "incorta-toolkit" )
			),
			array(
				"type" => "textarea",
				"heading" => esc_html__( "After Details Text", "incorta-toolkit" ),
				"param_name" => "after_incorta_desc",
				"value" => esc_html__( "After Details", "incorta-toolkit" ),
				"description" => esc_html__( "Type after details here.", "incorta-toolkit" )
			),
		)
	)
);