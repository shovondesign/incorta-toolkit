<?php 
/*
Plugin Name: Incorta Toolkit
Plugin URI: http://shovondesign.com
Description: After install the Incorta WordPress Theme, you must need to install this "Incorta Toolkit Plugin" first to get all functions of Incorta WP Theme.
Author: Tanvirul Haque
Author URI: http://www.shovondesign.com
Version: 1.0.0
Text Domain: incorta-toolkit
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// define
define('INCORTA_ACC_URL', WP_PLUGIN_URL . '/' . plugin_basename( dirname( __FILE__ ) ) . '/' );
define('INCORTA_ACC_PATH', plugin_dir_path( __FILE__ ) );

add_action( 'init', 'incorta_toolkit_custom_post' );
function incorta_toolkit_custom_post() {

    register_post_type( 'testimonials',
        array(
            'labels' => array(
                'name' => esc_html__( 'Testimonials', 'incorta-toolkit' ),
                'singular_name' => esc_html__( 'Testimonial', 'incorta-toolkit' )
            ),
            'supports' => array('title', 'editor', 'page-attributes'),
            'menu_icon'   => 'dashicons-testimonial',
            'public' => false,
            'show_ui' => true,
        )
    );
}

// Print Shortcode in widgets
add_filter('widget_text', 'do_shortcode');

// Loading VC addon
require_once( INCORTA_ACC_PATH . 'vc-addons/vc-blocks-load.php' );

// Theme Shortcodes
require_once( INCORTA_ACC_PATH . 'theme-shortcodes/home-banner-shortcode.php' );
require_once( INCORTA_ACC_PATH . 'theme-shortcodes/platform-shortcode.php' );
require_once( INCORTA_ACC_PATH . 'theme-shortcodes/btn-shortcode.php' );
require_once( INCORTA_ACC_PATH . 'theme-shortcodes/testimonial-shortcode.php' );
require_once( INCORTA_ACC_PATH . 'theme-shortcodes/logo-carousel-shortcode.php' );

//Registering Profi Toolkit File
function incorta_toolkit_files() {
    wp_enqueue_style('incorta-toolkit', plugin_dir_url( __FILE__ ) .'assets/css/incorta-toolkit.css');
}
add_action('wp_enqueue_scripts', 'incorta_toolkit_files');
