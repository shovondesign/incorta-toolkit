<?php if (!defined('ABSPATH')) die('-1');

function home_banner_shortcode( $atts, $content = null ){
    extract( shortcode_atts( array(
        'home_banner_bg' => '',
        'sec_title'	=> esc_html__( 'Home Benner Title', 'incorta-toolkit' ),
        'sec_subtitle' => esc_html__( 'Home Benner Sub-Title', 'incorta-toolkit' ),
        'youtube_embed_code' => 'RYvNSA1ww6g',
        'mc_shortcode_get_id' => '',
    ), $atts) );

    $banner_bg = wp_get_attachment_image_url($home_banner_bg, 'large');
	$incorta_mc_from = do_shortcode('[mc4wp_form id="'.$mc_shortcode_get_id.'"]');

    $home_banner_markup = '
	<section class="header-banner-area text-center" id="home" style="background:url('.esc_url( $banner_bg ).');">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-sm-12">
					<div class="header-banner-text">';

					if ( !empty( $sec_title ) ) {
		                $home_banner_markup .='<h1>'.esc_html( $sec_title ).'</h1>';
		            } else {
		                $home_banner_markup .='';
		            }
		            if ( !empty( $sec_subtitle ) ) {
		                $home_banner_markup .=''.incorta_wp_kses( wpautop( $sec_subtitle ) ).'';
		            } else {
		                $home_banner_markup .='';
		            }

					$home_banner_markup .= '	
					</div>
					<div class="header-youtube-view">
						<iframe width="560" height="319" src="https://www.youtube.com/embed/'.$youtube_embed_code.'?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>

					<div class="header-signup">';

					if ( !empty( $incorta_mc_from )) {
						$home_banner_markup .=''.$incorta_mc_from.'';
					} else {
						$home_banner_markup .='';
					}

					$home_banner_markup .= '
					</div>
				</div>
			</div>
		</div>		
	</section>
    ';

    return $home_banner_markup;
}
add_shortcode('incorta_home_banner', 'home_banner_shortcode');