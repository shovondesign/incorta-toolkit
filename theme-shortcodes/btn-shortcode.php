<?php if (!defined('ABSPATH')) die('-1');

function incorta_btn_shortcode( $atts, $content = null ){
    extract( shortcode_atts( array(
        'btn_text'  => esc_html__( 'Button', 'blacki-toolkit' ),
        'btn_icon'  => '',
        'btn_link'	=> '#',
        'btn_type'	=> '',
    ), $atts) );

    $btn_markup = '<a href="'.esc_url( $btn_link ).'" class="button '.esc_attr($btn_type).'">';
    if ( !empty( $btn_icon ) ) { $btn_markup .= '<i class="'.esc_attr( $btn_icon ).'"></i>'; }
    $btn_markup .= ''.esc_html( $btn_text ).'</a>';

    return $btn_markup;
}
add_shortcode('incorta_btn', 'incorta_btn_shortcode');