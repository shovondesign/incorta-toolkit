<?php if (!defined('ABSPATH')) die('-1');

function incorta_logo_carousel_shortcode($atts, $content = null) {

	extract( shortcode_atts(array(
		'sec_title'	=> esc_html( "Section Title", "incorta-toolkit" ),
		'logos'	=> '',
	), $atts) );

	$logo_ids = explode(',', $logos);

	$logo_carousel_markup = '

	<section class="clients-area section-padding text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>'.esc_html( $sec_title ).'</h2>
					<div id="logo-carousel" class="text-center">';

					foreach ($logo_ids as $logo) {
						$logo_array = wp_get_attachment_image_src($logo, 'large');
						$logo_carousel_markup .= '
						<div class="item">
							<div class="logo-carousel-item-tabel">
								<div class="logo-carousel-item-tabelcell text-center">
								<img src="'.$logo_array[0].'" class="img-responsive" alt="'.$logo_array[1].'">
								</div>
							</div>
						</div>
						';
					}

					$logo_carousel_markup .= '
					</div>
				</div>
			</div>
		</div>
	</section>
	';
	return $logo_carousel_markup;
}
add_shortcode('incorta_logo_carousel', 'incorta_logo_carousel_shortcode');