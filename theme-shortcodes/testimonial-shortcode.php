<?php if (!defined('ABSPATH')) die('-1');

function incorta_testimonial_shortcode( $atts, $content = null ){
    extract( shortcode_atts( array(
    	'testimonial_bg'		=> '',
        'before_incorta_title'	=> esc_html__( 'Before Title', 'incorta-toolkit' ),
        'before_incorta_desc'	=> esc_html__( 'Before Details', 'incorta-toolkit' ),
        'after_incorta_title'	=> esc_html__( 'After Title', 'incorta-toolkit' ),
        'after_incorta_desc'	=> esc_html__( 'After Details', 'incorta-toolkit' ),
    	'count'		=> -1,
    ), $atts) );

    $testimonial_bg_link = wp_get_attachment_image_url($testimonial_bg, 'large');

    $testimonial_markup = '
	<section class="testimonial-area section-padding" style="background:url('.esc_url( $testimonial_bg_link ).');">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-2 col-xs-12">
					<div id="testimonial-carousel">';

						$testimonial_array = new WP_Query( array( 'posts_per_page' => $count, 'post_type' => 'testimonials' ) );

						while($testimonial_array->have_posts()) : $testimonial_array->the_post();

							$testimonial_markup .= '
							<div class="item single-testimonial">
								<h2>'.esc_html( get_the_content() ).'</h2>
								<p>'.esc_html( get_the_title() ).'</p>
							</div>
							';
						
						endwhile;
						wp_reset_query();

					$testimonial_markup .= '

					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="before-after-incora">
						<h4>'.$before_incorta_title.'</h4>
						<p>'.$before_incorta_desc.'</p>
					</div>
				</div>
				<div class="col-md-6"></div>
				<div class="col-md-3 col-sm-6">
					<div class="before-after-incora text-right">
						<h4>'.$after_incorta_title.'</h4>
						<p>'.$after_incorta_desc.'</p>
					</div>
				</div>
			</div>
		</div>
	</section>
    ';

    return $testimonial_markup;
}
add_shortcode('incorta_testimonial', 'incorta_testimonial_shortcode');