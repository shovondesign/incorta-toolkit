<?php if (!defined('ABSPATH')) die('-1');

function incorta_platform_shortcode( $atts, $content = null ){
    extract( shortcode_atts( array(
        'platform_heading'  => esc_html__( 'Heading', 'incorta-toolkit' ),
        'platform_subheading'  => esc_html__( 'Sub-Heading', 'incorta-toolkit' ),
        'platform_img'	=> '',
        'platform_details'  => esc_html__( 'Details', 'incorta-toolkit' ),
    ), $atts) );

	$platform_img_link = wp_get_attachment_image_url($platform_img, 'large');

    $platform_markup = '
		<div class="single-platform">
			<h3>'.$platform_heading.'</h3>
			<h4>'.$platform_subheading.'</h4>
			<img src="'.esc_url( $platform_img_link ).'" alt="'.$platform_heading.'" class="img-responsive">
			<p>'.$platform_details.'</p>
		</div>
    ';

    return $platform_markup;
}
add_shortcode('incorta_platform', 'incorta_platform_shortcode');